<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Form Validation in Laravel</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>

<body>
    <div class="container mt-5">

        <!-- Success message -->
        @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
        @endif

        <form action="" method="post" action="{{ action('App\Http\Controllers\ReservController@createReservForm') }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label> Nom</label>
                <input type="text" class="form-control {{ $errors->has('nom') ? 'error' : '' }}" name="nom" id="nom">

                <!-- Error -->
                @if ($errors->has('Nom'))
                <div class="error">
                    {{ $errors->first('Nom') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label> Prenom</label>
                <input type="text" class="form-control {{ $errors->has('prenom') ? 'error' : '' }}" name="prenom" id="prenom">

                <!-- Error -->
                @if ($errors->has('Prenom'))
                <div class="error">
                    {{ $errors->first('Prenom') }}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label> CIN</label>
                <input type="text" class="form-control {{ $errors->has('cin') ? 'error' : '' }}" name="cin" id="cin">

                <!-- Error -->
                @if ($errors->has('CIN'))
                <div class="error">
                    {{ $errors->first('CIN') }}
                </div>
                @endif
            </div>




            <div class="form-group">
                <label>Téléphone</label>
                <input type="text" class="form-control {{ $errors->has('telephone') ? 'error' : '' }}" name="telephone" id="telephone">

                @if ($errors->has('telephone'))
                <div class="error">
                    {{ $errors->first('telephone') }}
                </div>
                @endif
            </div>

            <div class="form-group">
                <label>Adresse</label>
                <textarea class="form-control {{ $errors->has('adresse') ? 'error' : '' }}" name="adresse" id="adresse" rows="4"></textarea>

                @if ($errors->has('adresse'))
                <div class="error">
                    {{ $errors->first('adresse') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label> Service</label>
                <input type="text" class="form-control {{ $errors->has('service') ? 'error' : '' }}" name="service" id="service">

                <!-- Error -->
                @if ($errors->has('Service'))
                <div class="error">
                    {{ $errors->first('Service ') }}
                </div>
                @endif
            </div>

            <input type="submit" name="send" value="Submit" class="btn btn-dark btn-block">
        </form>
    </div>
</body>

</html>