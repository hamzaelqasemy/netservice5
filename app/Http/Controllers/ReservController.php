<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\reservation;
use Illuminate\Support\Facades\Redirect;


class ReservController extends Controller
{
    // Create Form
    public function createReservForm(Request $request)
    {
        return view('reservation');
    }

    // Store Form data in database
    public function ReservForm(Request $request)
    {

        // Form validation
        /* $this->validate($request, [
            'nom' => 'required',
            'prenom' => 'required',
            'cin' => 'required',
            'telephone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:8',
            'adresse' => 'required',
            'service' => 'required'
        ]);*/
        $reservation = new reservation();
        $reservation->nom = $request->input('nom');
        $reservation->prenom = $request->input('prenom');
        $reservation->cin = $request->input('cin');
        $reservation->telephone = $request->input('telephone');
        $reservation->adresse = $request->input('adresse');
        $reservation->service = $request->input('service');
        $reservation->save();
        //  Store data in database
        /*reservation::create($request->all());

        return back()->with('success', 'Les données ont été enregistrées avec succès.');*/
        return Redirect('reservation');
    }
}
