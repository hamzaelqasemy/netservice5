<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/reservation', function () {
    return view('reservation');
});

Route::get('/reservation', ['App\Http\Controllers\ReservController', 'createReservForm']);
Route::post('/reservation', ['App\Http\Controllers\ReservController', 'ReservForm']);
/*Route::post('/reservation', [
    'uses' => 'ReservController@ReservForm',
    'as' => 'reservation.ReservForm'
]);
Route::post('/', [\App\Http\Controllers\ReservController::class, 'reservation'])->name('reservation');  */

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
